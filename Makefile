LATEX			:= lualatex
FILENAME	:= scoresheet

all:
	${LATEX} ${FILENAME}

clean:
	rm -rf *.toc *.out *.run.xml *.pdf *.aux *.log
