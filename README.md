# Fantasy General Conference

Fantasy [General Conference](https://www.churchofjesuschrist.org/general-conference?lang=eng)

To learn more about General Conference, visit the [website](https://www.churchofjesuschrist.org/general-conference?lang=eng) of the Church of Jesus Christ of Latter-Day Saints. To suggest improvements or new sections, open an [issue](https://gitlab.com/cwpitts/fantasy-gc/-/issues).
